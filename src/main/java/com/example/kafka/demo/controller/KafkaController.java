package com.example.kafka.demo.controller;

import com.example.kafka.demo.services.ProducerClass;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.internals.Topic;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class KafkaController {

    private final ProducerClass producer;

    public KafkaController(ProducerClass producer) {
        this.producer = producer;
    }
    @PostMapping("/publish")
    public String sendMessage(@RequestParam("message") String message){
        producer.sendMessage(message);
        return "Published successfully";
    }

    @Bean
    public NewTopic adviceTopic(){
        return new NewTopic("user", 3,(short)1);
    }
}
